<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Projects;
use Redirect;
use App\User;
use Validator;
use File; 
use Session;
use App\Documents;
use Illuminate\Support\Str;

class VisualizationController extends Controller {
	public function index(Request $request) {
	if (Auth::id ()) {
			$user_id = Auth::id ();
			$user = User::find ( $user_id );
			$projects = Projects::where ( 'user_id', $user_id )->where ( 'project_type', 'visualize' )->get ();
			return view ( 'user.visualization', compact ( 'user', 'projects' ) );
		} else 

		{
			return view ( 'pages.visualize' );
		}
	}
	public function createProject(Request $request) {
		$user_id = Auth::id ();
		$project = new Projects ();
		$project->user_id = $user_id;
		$project->project_name = $request->get ( 'project_name' );
		$project->project_type = "visualize";
		//$out = shell_exec ( 'start /B test.bat' );
		$project->save ();
		return Redirect::back ();
	}
	public function getProjects(Request $req) {
		$proj_docs = Documents::where ( 'proj_id', $req->project_id )->where ( 'doc_type', 'logFile' )->get ();
		return response ()->json ( $proj_docs );
	}
	public function uploadLogfiles(Request $request, $pid) {
		// added jtl extension too for validation
		$rules = array (
				'logfiles' => 'required',
				'logfiles_extension' => 'in:csv,log,jtl' 
		);
		$messsages = array (
				'logfiles.required' => 'Log file is required.',
				'logfiles_extension.in' => 'Log file must be only of csv, log or jtl extension' 
		);
		$validator = Validator::make ( [ 
				'logfiles' => $request->file ( 'logfiles' ),
				'logfiles_extension' => strtolower ( $request->file ( 'logfiles' )->getClientOriginalExtension () ) 
		], $rules, $messsages );
		if ($validator->fails ())
			return Redirect::back ()->withErrors ( $validator );
		else {
			$user_id = Auth::id ();
			$user = User::find ( $user_id );
			$project = Projects::findOrFail ( $pid );
			if ($request->hasFile ( 'logfiles' )) {
				$file = $request->file ( 'logfiles' );
				$original_name = $file->getClientOriginalName ();
				$extension = $file->getClientOriginalExtension ();
				$mime = $file->getClientMimeType ();
				$filename = $file->getFilename ();
				// Chamged document url user_id/projects/proj_id/dashartifacts
				$document_url = $user->id . '/projects/' . $project->id . '/dashartifacts' . '/' . $filename . '.' . $extension;
				$document_filename = $original_name;
				$file->move ( $user->id . '/projects/' . $project->id . '/dashartifacts/', $filename . '.' . $extension );
				$document = new Documents ();
				$document->user_id = $user_id;
				$document->doc_name = $document_filename;
				$document->doc_path = $document_url;
				$document->proj_id = $project->id;
				$project->log_file = 1;
				$document->doc_type = 'logFile';
				$logFile = file ( $document_url );
				$headersLine = $logFile [0];
				for($i = 1; $i < count ( $logFile ); $i ++) {
					$rr = explode ( ',', $logFile [$i] );
					foreach ( $rr as $key => $value )
						$eachline [$key] [] = $value;
				}
				$timeStamps = array_unique($eachline [0]);
				foreach($timeStamps as $timestamp){
					$timestamp = substr ( $timestamp, 0, - 3 );
					$date[] = ( date("Y-m-d\TH:i:s\Z", $timestamp));
				}
				$maxDate = max(( $date));
				$minDate = min(( $date));
				$threadGroup_array = array_unique ( $eachline [5] );
				$label_array = array_unique ( $eachline [2] );
				$title = $project->project_name;
				$datasource = "Datasource";
				$label_name = "Sample Label name";
				$labels = array_values($label_array);
				$refId = 'A';
				$dashboard = '
{
  "id": 9,
  "title": "' . $title . '",
  "tags": [],
  "style": "dark",
  "timezone": "browser",
  "editable": true,
  "hideControls": false,
  "sharedCrosshair": false,
  "rows": [
    {
      "collapse": false,
      "editable": true,
      "height": "250px",
      "panels": [
        {
          "aliasColors": {},
          "bars": false,
          "datasource": "' . $datasource . '",
          "editable": true,
          "error": false,
          "fill": 1,
          "grid": {
            "threshold1": null,
            "threshold1Color": "rgba(216, 200, 27, 0.27)",
            "threshold2": null,
            "threshold2Color": "rgba(234, 112, 112, 0.22)"
          },
          "height": "500",
          "id": 2,
          "isNew": true,
          "legend": {
            "avg": false,
            "current": false,
            "hideEmpty": true,
            "hideZero": false,
            "max": false,
            "min": false,
            "show": true,
            "total": false,
            "values": false
          },
          "lines": true,
          "linewidth": 2,
          "links": [],
          "nullPointMode": "connected",
          "percentage": false,
          "pointradius": 5,
          "points": false,
          "renderer": "flot",
          "seriesOverrides": [
            {
              "alias": "Business_Other",
              "yaxis": 1
            }
          ],
          "span": 12,
          "stack": false,
          "steppedLine": false,'; foreach ($labels as $ar)
          	$dashboard .= '
          "targets": [
            {
              "alias": "' .  $ar . '", "bucketAggs": [{   "field": "@timestamp","id": "2","settings": {"interval": "auto","min_doc_count": 0,"trimEdges": 0},"type": "date_histogram"}],
              "dsType": "elasticsearch",
              "metrics": [{"field": "elapsed","id": "1","meta": {},"settings": {},"type": "avg"}],
              "query": "label:' . Str::studly ( $ar ) . '",
              "refId": "' . $refId ++ . '",
              "timeField": "@timestamp"
            }
          ],';
          $dashboard .= '    		
          "timeFrom": null,
          "timeShift": null,
          "title": "Average Elapsed Time",
          "tooltip": {
            "msResolution": true,
            "shared": true,
            "sort": 0,
            "value_type": "cumulative"
          },
          "type": "graph",
          "xaxis": {
            "show": true
          },
          "yaxes": [
            {
              "format": "ms",	"label": null,	"logBase": 1,	"max": null,	"min": null,	"show": true
            },
            {
              "format": "short",	"label": null,	"logBase": 1,	"max": null,	"min": null,	"show": true
            }
          ]
        }
      ]
    }
  ],
  "time": {
    "from": "'.$minDate.'",
    "to": "'.$maxDate.'"
  },
  "timepicker": {
    "refresh_intervals": [
      "5s",
      "10s",
      "30s",
      "1m",
      "5m",
      "15m",
      "30m",
      "1h",
      "2h",
      "1d"
    ],
    "time_options": [
      "5m",
      "15m",
      "1h",
      "6h",
      "12h",
      "24h",
      "2d",
      "7d",
      "30d"
    ]
  },
  "templating": {
    "list": []
  },
  "annotations": {
    "list": []
  },
  "refresh": false,
  "schemaVersion": 12,
  "version": 0,
  "links": [],
  "gnetId": null
}
						
						';
				File::put ( $user->id . '/projects/' . $project->id . '/dashartifacts/dashboard.json', $dashboard );
				$headersLine = str_replace ( "\n", "", $headersLine );
				$headers = explode ( ",", $headersLine );
				$headersPresent = array (
						"timeStamp",
						"elapsed",
						"label",
						"responseCode",
						"responseMessage",
						"threadName",
						"dataType",
						"success",
						"bytes",
						"grpThreads",
						"allThreads",
						"Latency",
						"IdleTime" 
				);
				$arr_diff = array_diff ( $headersPresent, $headers );
				if (count ( $arr_diff ) > 0) {
					foreach ( $arr_diff as $missing_header )
						$message_missing_header [] = $missing_header;
					$message = "Missing headers : '" . implode ( ",", $message_missing_header ) . "'. So please check the log file headers an re-upload again.";
					Session::flash ( 'logError-message', $message );
					return Redirect::back ();
				} else {
					// made hosts and index as variables
					$now = new \DateTime ();
					$hosts = "127.0.0.1";
					$contents = '# Variables : Index Name : Index Name should be the timestamp and the User ID : UserID_timeStamp_testID
# Variable : Path Name : Path Name should be the <Timestamp>_<TestID>.csv
	
input {
    file {
        path => ' . $document_url . '
        start_position => "beginning"
    }
}
	
filter {
	
  if ([message] =~ "responseCode") {
    drop { }
  } else {
    csv {
        columns => ["' . implode ( "\",\"", $headers ) . '"]
    }
    date {
      match => [ "timeStamp", "UNIX_MS" ]
    }
	
    mutate {
      convert => [ "elapsed", "integer" ]
      convert => [ "latency", "integer" ]
      convert => [ "bytes", "integer" ]
	  convert => [ "grpThreads", "integer" ]
	  convert => [ "allThreads", "integer" ]
	  convert => [ "SampleCount", "integer" ]
	  convert => [ "ErrorCount", "integer" ]
	  convert => [ "IdleTime", "integer" ]
	  convert => [ "Connect", "integer" ]
   
    }
  }
}
output {
#  stdout { codec => rubydebug }
  elasticsearch {
    hosts => "' . $hosts . '"
    index => "' . $user->id . '_' . $project->id . '_' . $now->getTimestamp () . '"
  }
} ';
					// storing a conf file
					File::put ( $user->id . '/projects/' . $project->id . '/dashartifacts/' . $filename . '.conf', $contents );
					$project->update ();
					$document->save ();
					$document = new Documents ();
					$document->user_id = $user_id;
					$document->doc_name = $filename;
					$document->doc_path = $user->id . '/projects/' . $project->id . '/dashartifacts' . '/' . $filename . '.conf';
					$document->proj_id = $project->id;
					$document->doc_type = 'confFile';
					Session::flash ( 'success-message', 'File(s) uploaded successfully.' );
					return Redirect::back ();
				}
			}
		}
	}
}
